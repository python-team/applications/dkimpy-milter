Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: dkimpy-milter
Source: https://launchpad.net/dkimpy-milter

Files: *
Copyright: 2018 Scott Kitterman <scott@kitterman.com>
License: GPL-2

Files: dkimpy_milter/__init__.py
Copyright: 2018 Scott Kitterman <scott@kitterman.com>
           2007 Business Management Systems, Inc.
License: GPL-2

Files: dkimpy_milter/util.py
Copyright: 2018 Scott Kitterman
           2016  NigelB
License: GPL-2+

Files: dkimpy_milter/config.py
Copyright: 2007-2018 Scott Kitterman <scott@kitterman.com>
           2004-2005, Sean Reifschneider, tummy.com, ltd.
License: GPL-2

Files: man/*
Copyright: 2018 Scott Kitterman <scott@kitterman.com>
           2009-2015, The Trusted Domain Project. 
           2007, 2008, Sendmail, Inc. and its suppliers.
License: GPL-2 and Sendmail and BSD-3-Clause 

License: GPL-2
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: Sendmail
                   SENDMAIL OPEN SOURCE LICENSE
 .
 The following license terms and conditions apply to this open source
 software ("Software"), unless a different license is obtained directly
 from Sendmail, Inc. ("Sendmail") located at 6475 Christie Ave, Suite 350,
 Emeryville, CA 94608, USA.
 .
 Use, modification and redistribution (including distribution of any
 modified or derived work) of the Software in source and binary forms is
 permitted only if each of the following conditions of 1-6 are met:
 .
 1. Redistributions of the Software qualify as "freeware" or "open
    source software" under one of the following terms:
 .
    (a) Redistributions are made at no charge beyond the reasonable
        cost of materials and delivery; or
 .
    (b) Redistributions are accompanied by a copy of the modified
        Source Code (on an acceptable machine-readable medium) or by an
        irrevocable offer to provide a copy of the modified Source Code
        (on an acceptable machine-readable medium) for up to three years
        at the cost of materials and delivery. Such redistributions must
        allow further use, modification, and redistribution of the Source
        Code under substantially the same terms as this license. For
        the purposes of redistribution "Source Code" means the complete
        human-readable, compilable, linkable, and operational source
        code of the redistributed module(s) including all modifications.
 .
 2. Redistributions of the Software Source Code must retain the
    copyright notices as they appear in each Source Code file, these
    license terms and conditions, and the disclaimer/limitation of
    liability set forth in paragraph 6 below. Redistributions of the
    Software Source Code must also comply with the copyright notices
    and/or license terms and conditions imposed by contributors on
    embedded code. The contributors' license terms and conditions
    and/or copyright notices are contained in the Source Code
    distribution.
 .
 3. Redistributions of the Software in binary form must reproduce the
    Copyright Notice described below, these license terms and conditions,
    and the disclaimer/limitation of liability set forth in paragraph
    6 below, in the documentation and/or other materials provided with
    the binary distribution.  For the purposes of binary distribution,
    "Copyright Notice" refers to the following language: "Copyright (c)
    1998-2009 Sendmail, Inc. All rights reserved."
 .
 4. Neither the name, trademark or logo of Sendmail, Inc. (including
    without limitation its subsidiaries or affiliates) or its contributors
    may be used to endorse or promote products, or software or services
    derived from this Software without specific prior written permission.
    The name "sendmail" is a registered trademark and service mark of
    Sendmail, Inc.
 .
 5. We reserve the right to cancel this license if you do not comply with 
    the terms.  This license is governed by California law and both of us 
    agree that for any dispute arising out of or relating to this Software, 
    that jurisdiction and venue is proper in San Francisco or Alameda 
    counties.  These license terms and conditions reflect the complete 
    agreement for the license of the Software (which means this supercedes 
    prior or contemporaneous agreements or representations).  If any term
    or condition under this license is found to be invalid, the remaining
    terms and conditions still apply.
 .
 6. Disclaimer/Limitation of Liability: THIS SOFTWARE IS PROVIDED BY
    SENDMAIL AND ITS CONTRIBUTORS "AS IS" WITHOUT WARRANTY OF ANY KIND
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY, NON-INFRINGEMENT AND FITNESS FOR A
    PARTICULAR PURPOSE ARE EXPRESSLY DISCLAIMED. IN NO EVENT SHALL SENDMAIL
    OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
    TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
    OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
    OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
    WITHOUT LIMITATION NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
    USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 .
 $Revision: 1.1 $ $Date: 2009/07/16 18:43:18 $

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
     * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.
     * Neither the name of The Trusted Domain Project nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE TRUSTED DOMAIN PROJECT ''AS IS'' AND ANY
 EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE TRUSTED DOMAIN PROJECT BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


