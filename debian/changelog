dkimpy-milter (1.2.3-3) unstable; urgency=medium

  * Team Upload
  * Use dh-sequence-python3
  * Set DPT as Maintainer per new Team Policy
  * Drop dependency on python3-pkg-resources (Closes: #1083368)

 -- Alexandre Detiste <tchet@debian.org>  Sun, 12 Jan 2025 18:28:39 +0100

dkimpy-milter (1.2.3-2) unstable; urgency=medium

  * Add d/source/options extend-diff-ignore to fix dpkg-source failure due to
    local changes (python package metadata regeneration) (Closes: #1044966)
  * Correct repository location in Vcs-Git
  * Update d/rules to not move dkimpy-milter.service to /lib to support
    usr-move (Closes: #1073730)
  * Bump standards-version to 4.7.0 without further change
  * Add Rules-Requires-Root: no to d/control

 -- Scott Kitterman <scott@kitterman.com>  Tue, 18 Jun 2024 09:17:18 -0400

dkimpy-milter (1.2.3-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Set upstream metadata fields: Repository-Browse.
  * Changed vcs type from git to Git based on URL.
  * Remove constraints unnecessary since buster (oldstable):
    + Build-Depends: Drop versioned constraint on python3, python3-authres and
      python3-dnspython.
    + dkimpy-milter: Drop versioned constraint on python3-authres and
      python3-dnspython in Depends.

  [ Scott Kitterman ]
  * New upstream release, (Closes: #958388, #969215, #981156, #981157,
    #995335)
  * Drop Suggests on obsolete package lsb-base
  * Drop alternate build-depends/depends on python3-dns - dnspython is a
    better choice now
  * Bump standards-version to 4.6.2 without further change
  * Set minimum python3-milter and python3-dkim versions per upstream

 -- Scott Kitterman <scott@kitterman.com>  Sun, 26 Feb 2023 20:39:43 -0500

dkimpy-milter (1.2.2-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * No source change upload to rebuild with debhelper 13.10.

 -- Michael Biebl <biebl@debian.org>  Sat, 15 Oct 2022 12:01:08 +0200

dkimpy-milter (1.2.2-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

 -- Sandro Tosi <morph@debian.org>  Tue, 14 Sep 2021 23:43:07 -0400

dkimpy-milter (1.2.2-1) unstable; urgency=medium

  * New upstream release
  * Update debhelper compat to 12
  * Bump standards-version to 4.5.0 without further change
  * Added misc:Pre-Depends for init-system-helpers (thanks Lintian)

 -- Scott Kitterman <scott@kitterman.com>  Sun, 09 Aug 2020 15:26:51 -0400

dkimpy-milter (1.2.1-1) unstable; urgency=medium

  * New upstream release
  * Drop existing patches, OBE by upstream changes
  * d/rules: Update to set Debian paths using setup.py expand and for new
    config file location
  * d/docs: Update for rename of README to README.md
  * Add d/maintscript to move dkimpy-milter.conf to new location
  * Bump minimum version requirements for python3-dkim and python3-dnspython
  * d/control: d/rules: explicitly specify depends so python3-dns alternate
    depends is provided

 -- Scott Kitterman <scott@kitterman.com>  Sat, 04 Jan 2020 14:48:47 -0500

dkimpy-milter (1.1.4-1) unstable; urgency=medium

  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Fri, 22 Nov 2019 20:06:09 -0500

dkimpy-milter (1.1.3-1) unstable; urgency=medium

  * New upstream release
  * Add d/p/0002-update-upstream-init-paths.patch so the sysv init file can
    find the executable and the config file
  * Bump standards-version to 4.4.1 without further change

 -- Scott Kitterman <scott@kitterman.com>  Sun, 06 Oct 2019 01:37:34 -0400

dkimpy-milter (1.1.2-1) unstable; urgency=medium

  * New upstream release
  * Put upstream init file where dh_installinit expects to find it so it is
    properly registered
  * Update public upstream key to be minimal

 -- Scott Kitterman <scott@kitterman.com>  Mon, 23 Sep 2019 16:45:07 -0400

dkimpy-milter (1.1.1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Scott Kitterman ]
  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Fri, 06 Sep 2019 01:12:22 -0400

dkimpy-milter (1.1.0-2) unstable; urgency=medium

  * Upload to unstable
  * Bump standards version to 4.4.0 without further change

 -- Scott Kitterman <scott@kitterman.com>  Sun, 07 Jul 2019 15:40:56 -0400

dkimpy-milter (1.1.0-1) experimental; urgency=medium

  * New upstream release (Closes: #922006)
    - Updated patches
  * Update d/control and d/rules for switch to python3

 -- Scott Kitterman <scott@kitterman.com>  Sat, 13 Apr 2019 00:52:00 -0400

dkimpy-milter (1.0.1-1) unstable; urgency=medium

  * New upstream release
  * Bump standards-version to 4.3.0 without further change
  * Update long description to account for Ed25519 RFC being published

 -- Scott Kitterman <scott@kitterman.com>  Mon, 11 Feb 2019 15:32:17 -0500

dkimpy-milter (1.0.0-1) unstable; urgency=medium

  * New upstream release
  * Bump standards-version to 4.1.4 without further change

 -- Scott Kitterman <scott@kitterman.com>  Fri, 11 May 2018 19:05:57 -0400

dkimpy-milter (0.9.7-1) unstable; urgency=medium

  * New upstream release
  * Fix GPL-2 header in debian/copyright
  * Add python-authres and python-dkim it pydist-overrides so depends are
    properly versioned

 -- Scott Kitterman <scott@kitterman.com>  Mon, 19 Mar 2018 01:17:58 -0400

dkimpy-milter (0.9.6-1) unstable; urgency=medium

  * Initial release (Closes: #892740)

 -- Scott Kitterman <scott@kitterman.com>  Thu, 15 Mar 2018 18:54:13 -0400
